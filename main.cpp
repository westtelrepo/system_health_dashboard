#include "beacon.h"

#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <cstdint>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTranslator translator;
    const QStringList uiLanguages = QLocale::system().uiLanguages();
    for (const QString &locale : uiLanguages) {
        const QString baseName = "Beacon_" + QLocale(locale).name();
        if (translator.load(":/i18n/" + baseName)) {
            a.installTranslator(&translator);
            break;
        }
    }

    Beacon w;
    if(!w.InternetPingCheckPre()){
        qDebug()<<"Error with the internet connection";
        exit(0);
    }
    w.readServerTable();
    w.beginTimer();
    w.createTableFormat();
    w.show();
    return a.exec();
}
