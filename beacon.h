#ifndef BEACON_H
#define BEACON_H

#include <QMainWindow>
#include<QAbstractTableModel>
#include<QVariant>
#include<QDebug>
#include<QtXml>
#include<QDomDocument>
#include<QTime>
#include<QString>
#include<QtGui>
#include<QFile>
#include<QTextStream>
#include<QUdpSocket>
#include<QProcess>
#include<QTimer>
#include"linuxheader.h"
#include"ServerData.h"
#include"westtelemail.h"
#include"alertgeneralizeemail.h"

using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class Beacon; }
QT_END_NAMESPACE


class Beacon : public QMainWindow, public ServerData, public WesttelEmail, public AlertGeneralizeEmail
{
    Q_OBJECT

public:
    Beacon(QWidget *parent = nullptr);
    ~Beacon();
    //ServerData	ServerDataTable[MAX_ROWS];
    map<QString,ServerData> ServerDataTable;
    void readServerTable();
    void printServerTable();
    void beginTimer();
    void createTableFormat();
    //These logic function alos update the column colors along with the string to be displayed
    pair<QString,int> PhoneStatuslogicCheck(QString,QString);
    pair<QString,int> TrunkStatuslogicCheck(QString,QString);
    pair<QString,int> ServerStatuslogicCheck(QString);
    pair<QString,int> ALIStatuslogicCheck(QString,QString,QString,QString);
    pair<QString,int> WorkStationLogicCheck(QString,QString);
    void timerEvent(QTimerEvent *event);
    void updateCompleteTable();
    void updateColumnColor(int,int,QString,pair<QString,int>);
    void updateErrorColumnColour(int,int,QString,pair<QString,int>);
    void updateErrorTable(vector<pair<QString,ServerData>> &);
    void closeEvent(QCloseEvent *event);
    int getWeight(QString);

private:
    Ui::Beacon  *ui;
    QUdpSocket  UDPsocket;
    QMutex		ServerDataMutex;
    int         myTimerId;



public slots:
    void ProcessPendingDatagrams();

};
#endif // BEACON_H
