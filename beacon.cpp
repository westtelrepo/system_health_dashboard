#include "beacon.h"
#include "ui_beacon.h"

Beacon::Beacon(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Beacon)
{
    ui->setupUi(this);
    //UDPsocket.bind(EXPERIENT_HOME_BEACON_PORT , QUdpSocket::ShareAddress);

    UDPsocket.bind(QHostAddress::AnyIPv4,EXPERIENT_HOME_BEACON_PORT);
    connect(&UDPsocket, SIGNAL(readyRead()), this, SLOT(ProcessPendingDatagrams()));
}

Beacon::~Beacon()
{
    delete ui;
}

//function --> to read the packets and store in the program respective data
void Beacon::ProcessPendingDatagrams()
{

    QByteArray       datagram;
    QHostAddress     SourceAddress;
    QString          strData, strServerName, strTransmitTime, strVersion, strXMLversion, strStratusStatus, strPhoneStatus, strTrunkStatus;
    QString          strALIRARecord, strALISVRecord, strALISARecord, strWorkStationStatus;
    QDomDocument     doc;
    QString          errorMsg;
    int              errorLine, errorColumn;


    while (UDPsocket.hasPendingDatagrams())
        {

         datagram.resize(UDPsocket.pendingDatagramSize());
         UDPsocket.readDatagram(datagram.data(), datagram.size(), &SourceAddress);
         //qDebug() << datagram.data() << " " << SourceAddress.toString();

         const QString    strInput = QString(datagram);

         if(!doc.setContent( strInput, &errorMsg, &errorLine, &errorColumn )){return;/*error box TBD*/}

        QDomElement EventTag = doc.namedItem("Event").toElement();
        if ( EventTag.isNull() )                {return;/*error box TBD*/}
        if ( EventTag.hasAttribute("XMLspec") ) {strXMLversion = EventTag.attribute("XMLspec");}

        QDomElement HeartbeatTag = EventTag.namedItem("Heartbeat").toElement();
        if ( HeartbeatTag.isNull() )                         {return;/*error box TBD*/}
        if ( HeartbeatTag.hasAttribute("ServerName") )       {strServerName = HeartbeatTag.attribute("ServerName");}
        if ( HeartbeatTag.hasAttribute("TransmitDateTime") ) {strTransmitTime =  HeartbeatTag.attribute("TransmitDateTime");}
        if ( HeartbeatTag.hasAttribute("Version") )          {strVersion = HeartbeatTag.attribute("Version");}
        if ( HeartbeatTag.hasAttribute("ST"))                {strStratusStatus = HeartbeatTag.attribute("ST");}
        if ( HeartbeatTag.hasAttribute("PH"))                {strPhoneStatus = HeartbeatTag.attribute("PH");}
        if ( HeartbeatTag.hasAttribute("T"))                 {strTrunkStatus = HeartbeatTag.attribute("T");}
        if ( HeartbeatTag.hasAttribute("RA"))                {strALIRARecord = HeartbeatTag.attribute("RA");}
        if ( HeartbeatTag.hasAttribute("SV"))                {strALISVRecord = HeartbeatTag.attribute("SV");}
        if ( HeartbeatTag.hasAttribute("SA"))                {strALISARecord = HeartbeatTag.attribute("SA");}
        if ( HeartbeatTag.hasAttribute("W"))                 {strWorkStationStatus = HeartbeatTag.attribute("W");}
        ServerDataMutex.lock();

        auto it=ServerDataTable.find(strServerName);
        if(it!=ServerDataTable.end()){
            ServerDataTable[strServerName].fSetTime();
            ServerDataTable[strServerName].IPaddress      = SourceAddress.toString();
            ServerDataTable[strServerName].Version        = strVersion;
            ServerDataTable[strServerName].XMLversion     = strXMLversion;
            ServerDataTable[strServerName].Expired        = false;

            ServerDataTable[strServerName].StratusStatus  = strStratusStatus;
            ServerDataTable[strServerName].colColors["StratusColColor"]  = ServerStatuslogicCheck(strStratusStatus);

            ServerDataTable[strServerName].colColors["PhoneColColor"]    = PhoneStatuslogicCheck(strPhoneStatus,strServerName);
            ServerDataTable[strServerName].colColors["TrunkColColor"]    = TrunkStatuslogicCheck(strTrunkStatus,strServerName);
            ServerDataTable[strServerName].colColors["ALIColColor"]      = ALIStatuslogicCheck(strALIRARecord,strALISARecord,strALISVRecord,strServerName);
            ServerDataTable[strServerName].colColors["WorkStColColor"] = WorkStationLogicCheck(strWorkStationStatus,strServerName);
            ServerDataTable[strServerName].fSetTime();
        }
        ServerDataMutex.unlock();
       }

}

//function  -->  logic behind the workstation desplay is present here
pair<QString,int> Beacon::WorkStationLogicCheck(QString input, QString strServerName){
    if(input==""){
        ServerDataTable[strServerName].WorkstationStatus = "";
        return {"WHITE",WHITE_COLOR_WEIGHT};                  //checking for the condition if the tag is missing from the xml
    }
    QString strnumberOfWorkStationRegistered;
    strnumberOfWorkStationRegistered.append(input.at(0));
    strnumberOfWorkStationRegistered.append(input.at(1));
    QString strTotalnumberOfWorkStation;
    strTotalnumberOfWorkStation.append(input.at(2));
    strTotalnumberOfWorkStation.append(input.at(3));


    int numberOfWorkStationRegistered =  strnumberOfWorkStationRegistered.toInt();
    int numberOfTotalWorkStation = strTotalnumberOfWorkStation.toInt();

    QString strResult;
    pair<QString,int> colResult;

    strResult.append(QString::number(numberOfWorkStationRegistered));
    strResult.append("/");
    strResult.append(QString::number(numberOfTotalWorkStation));

    if(numberOfWorkStationRegistered==0){
         colResult={"RED",RED_COLOR_WEIGHT};
    }
    else{
        colResult={"WHITE",WHITE_COLOR_WEIGHT};
    }
    ServerDataTable[strServerName].WorkstationStatus = strResult;
    return colResult;

}

//function to check all the ALI specific diplay logic here, It include SALI, ALI and SVC logic as well
pair<QString,int> Beacon::ALIStatuslogicCheck(QString strALIRARecord,QString strALISARecord,QString strALISVRecord,QString strServerName){
    QString strResult;
    QString colResultcolor = "WHITE";
    int     colResultWeight= WHITE_COLOR_WEIGHT;


    if(!strALIRARecord.isEmpty()){
        //RA Logic
        //Note: there is a difference between ALI Pairs and ALI links. An ALI Pair is two Links
        QString strnumberOfALIPair;
        strnumberOfALIPair.append(strALIRARecord.at(0));
        strnumberOfALIPair.append(strALIRARecord.at(1));
        QString strnumberOfALILinks;
        strnumberOfALILinks.append(strALIRARecord.at(2));
        strnumberOfALILinks.append(strALIRARecord.at(3));

        int numberOfALIPairs =  strnumberOfALIPair.toInt();
        int numberOfALILinks =  strnumberOfALILinks.toInt();
        if(numberOfALILinks>0 && (numberOfALILinks==(2*numberOfALIPairs))){
                strResult.append(QString::number(numberOfALILinks));
                strResult.append("/");
                strResult.append(QString::number(2*numberOfALIPairs));
                colResultcolor = "WHITE";
                colResultWeight = WHITE_COLOR_WEIGHT;
        }
        else if(numberOfALIPairs>numberOfALILinks){
                strResult.append(QString::number(numberOfALILinks));
                strResult.append("/");
                strResult.append(QString::number(2*numberOfALIPairs));
                colResultcolor = "RED";
                colResultWeight = RED_COLOR_WEIGHT;
        }
        else{
            strResult.append(QString::number(numberOfALILinks));
            strResult.append("/");
            strResult.append(QString::number(2*numberOfALIPairs));
            colResultcolor = "YELLOW";
            colResultWeight = YELLOW_COLOR_WEIGHT;
        }
        strResult.append(" ALI  ");

    }
    if(!strALISARecord.isEmpty()){
        //SA Logic
        QString strnumberOfALILinks;
        strnumberOfALILinks.append(strALISARecord.at(0));
        strnumberOfALILinks.append(strALISARecord.at(1));
        QString strnumberOfALIUpLinks;
        strnumberOfALIUpLinks.append(strALISARecord.at(2));
        strnumberOfALIUpLinks.append(strALISARecord.at(3));

        int numberOfALILinks    =  strnumberOfALILinks.toInt();
        int numberOfALIUpLinks  =  strnumberOfALIUpLinks.toInt();
        if(numberOfALIUpLinks==numberOfALILinks){
                strResult.append(QString::number(numberOfALIUpLinks));
                strResult.append("/");
                strResult.append(QString::number(numberOfALILinks));
                if(colResultWeight<WHITE_COLOR_WEIGHT){
                    colResultcolor = "WHITE";
                    colResultWeight = WHITE_COLOR_WEIGHT;
                }
        }
        else{
                strResult.append(QString::number(numberOfALIUpLinks));
                strResult.append("/");
                strResult.append(QString::number(numberOfALILinks));
                // colResult={"WHITE",WHITE_COLOR_WEIGHT};
                if(colResultWeight<RED_COLOR_WEIGHT){
                    colResultcolor = "RED";
                    colResultWeight = RED_COLOR_WEIGHT;
                }
        }
        strResult.append(" SALI  ");
    }
    if(!strALISVRecord.isEmpty()){
        //SV logic
        QString strnumberOfALIServicePort;
        strnumberOfALIServicePort.append(strALISVRecord.at(0));
        strnumberOfALIServicePort.append(strALISVRecord.at(1));
        QString strnumberOfALIServiceUpPort;
        strnumberOfALIServiceUpPort.append(strALISVRecord.at(2));
        strnumberOfALIServiceUpPort.append(strALISVRecord.at(3));

        int numberOfALIServicePort =  strnumberOfALIServicePort.toInt();
        int numberOfALIServiceUpPort =  strnumberOfALIServiceUpPort.toInt();
        if(numberOfALIServiceUpPort==numberOfALIServicePort){
                strResult.append(QString::number(numberOfALIServiceUpPort));
                strResult.append("/");
                strResult.append(QString::number(numberOfALIServicePort));
                if(colResultWeight<WHITE_COLOR_WEIGHT){
                    colResultcolor = "WHITE";
                    colResultWeight = WHITE_COLOR_WEIGHT;
                }
        }
        else{
                strResult.append(QString::number(numberOfALIServiceUpPort));
                strResult.append("/");
                strResult.append(QString::number(numberOfALIServicePort));
                if(colResultWeight<RED_COLOR_WEIGHT){
                    colResultcolor = "RED";
                    colResultWeight = RED_COLOR_WEIGHT;
                }
        }
        strResult.append(" SVC  ");
    }
    ServerDataTable[strServerName].ALIStatus = strResult;
    pair<QString,int> colResult = {colResultcolor,colResultWeight};
    return colResult;
}


//function --> it is related to all the Trunk specific logic display
pair<QString,int> Beacon::TrunkStatuslogicCheck(QString input,QString strServerName){
    if(input==""){
        ServerDataTable[strServerName].TrunkStatus = "";
        return {"WHITE",WHITE_COLOR_WEIGHT};                  //checking for the condition if the tag is missing from the xml
    }
    QString strnumberOfAlarmingTrunks;
    strnumberOfAlarmingTrunks.append(input.at(0));
    strnumberOfAlarmingTrunks.append(input.at(1));
    QString strnumberOfTrunks;
    strnumberOfTrunks.append(input.at(2));
    strnumberOfTrunks.append(input.at(3));

    //The core logic to check the Phone Status
    int numberOfAlarmingTrunk =  strnumberOfAlarmingTrunks.toInt();
    int numberOfTotalTrunk = strnumberOfTrunks.toInt();
    int numberOfUpTrunks = numberOfTotalTrunk - numberOfAlarmingTrunk;
    QString strResult;
    pair<QString,int> colResult;

    if(numberOfAlarmingTrunk==0){
        strResult.append("No Alarm");
        colResult={"WHITE",WHITE_COLOR_WEIGHT};
    }

    else if(numberOfTotalTrunk!=0 && (numberOfAlarmingTrunk>=(numberOfTotalTrunk/2))){
        strResult.append(QString::number(numberOfUpTrunks));
        strResult.append("/");
        strResult.append(QString::number(numberOfTotalTrunk));
        colResult={"RED",RED_COLOR_WEIGHT};
    }
    else{
        strResult.append(QString::number(numberOfUpTrunks));
        strResult.append("/");
        strResult.append(QString::number(numberOfTotalTrunk));
        colResult={"YELLOW",YELLOW_COLOR_WEIGHT};
    }
    ServerDataTable[strServerName].TrunkStatus = strResult;
    return colResult;
}

//function --> it is related to all the Phone specific logic display
pair<QString,int> Beacon::PhoneStatuslogicCheck(QString input,QString strServerName){

    if(input==""){
        ServerDataTable[strServerName].PhoneStatus = "";
        return {"WHITE",WHITE_COLOR_WEIGHT};                  //checking for the condition if the tag is missing from the xml
    }
    QString strnumberOfWorkingPhones;
    strnumberOfWorkingPhones.append(input.at(0));
    strnumberOfWorkingPhones.append(input.at(1));
    QString strnumberOfTotalPhone;
    strnumberOfTotalPhone.append(input.at(2));
    strnumberOfTotalPhone.append(input.at(3));

    //The core logic to check the Phone Status
    int numberOfWorkingPhones =  strnumberOfWorkingPhones.toInt();
    int numberOfTotalPhone = strnumberOfTotalPhone.toInt();

    QString strResult;
    pair<QString,int> colResult;

    if(numberOfWorkingPhones==numberOfTotalPhone){
        strResult.append("No Alarm");
        colResult={"WHITE",WHITE_COLOR_WEIGHT};
    }

    else if(numberOfTotalPhone!=0 && ((numberOfTotalPhone/2)>=numberOfWorkingPhones)){
        strResult.append(QString::number(numberOfWorkingPhones));
        strResult.append("/");
        strResult.append(QString::number(numberOfTotalPhone));
        colResult={"RED",RED_COLOR_WEIGHT};
    }
    else{
        strResult.append(QString::number(numberOfWorkingPhones));
        strResult.append("/");
        strResult.append(QString::number(numberOfTotalPhone));
        colResult={"YELLOW",YELLOW_COLOR_WEIGHT};
    }
    ServerDataTable[strServerName].PhoneStatus = strResult;
    return colResult;
}

//function --> itis related to the assigning stratus logic
pair<QString,int> Beacon::ServerStatuslogicCheck(QString input){

    if(input.compare("FTSIMX")==0)
        return {"RED",RED_COLOR_WEIGHT};
    else if(input.compare("FTCONF")==0)
        return {"YELLOW",YELLOW_COLOR_WEIGHT};
    else if(input.compare("FTDUPX")==0)
        return {"WHITE",WHITE_COLOR_WEIGHT};
    else if(input.compare("24HRS")==0)
        return {"MAGENTA",MAGENTA_COLOR_WEIGHT};
    else if(input.compare("No Connection")==0)
        return {"YELLOW",YELLOW_COLOR_WEIGHT};
    else
        return {"WHITE",WHITE_COLOR_WEIGHT};
}

void Beacon:: createTableFormat(){
    QStringList hlable;
    hlable<<"  PSAP  "<<"  PSAP Name  "<<"  IP Address  "<<"  Controller Version  "<<"  System Status  "<<"  ALI Link Status  "<<" Trunk Status  "<<" Phone Status "<<"Workstation Status";
    ui->tableWidget->setRowCount(ServerDataTable.size());
    ui->tableWidget->setColumnCount(9);
    ui->tableWidget->setHorizontalHeaderLabels(hlable);
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);

    ui->tableWidget_2->setRowCount(ServerDataTable.size());
    ui->tableWidget_2->setColumnCount(9);
    ui->tableWidget_2->setHorizontalHeaderLabels(hlable);
    ui->tableWidget_2->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}


void Beacon::updateCompleteTable(){
    ui->tableWidget->clear();
    createTableFormat();
    int row=0;
    for(auto it=ServerDataTable.begin();it!=ServerDataTable.end();++it){
        int col=0;

        //Below colums are pretty basic and no need to do the colour fill
        ui->tableWidget->setItem(row,col,new QTableWidgetItem(it->second.ServerName));
        col+=1;

        ui->tableWidget->setItem(row,col,new QTableWidgetItem(it->second.CityName));
        col+=1;

        ui->tableWidget->setItem(row,col,new QTableWidgetItem(it->second.IPaddress));
        col+=1;

        ui->tableWidget->setItem(row,col,new QTableWidgetItem(it->second.Version));
        col+=1;

        //ui->tableWidget->setItem(row,col,new QTableWidgetItem(it->second.XMLversion));
        //col+=1;

        //Below coloums are needed to do the colour fill
        //ui->tableWidget->setItem(row,col,new QTableWidgetItem(it->second.StratusStatus));
        updateColumnColor(row,col,it->second.StratusStatus,it->second.colColors["StratusColColor"]);
        col+=1;

        updateColumnColor(row,col,it->second.ALIStatus,it->second.colColors["ALIColColor"]);
        col+=1;

        updateColumnColor(row,col,it->second.TrunkStatus,it->second.colColors["TrunkColColor"]);
        col+=1;

        updateColumnColor(row,col,it->second.PhoneStatus,it->second.colColors["PhoneColColor"]);
        col+=1;

        updateColumnColor(row,col,it->second.WorkstationStatus,it->second.colColors["WorkStColColor"]);
        row+=1;
    }
    QString size = QString::number(ServerDataTable.size());
    QString lable2Data = "Total Sites Count \n\n          "+size;
    ui->label->setText(lable2Data);
}
void Beacon::updateErrorColumnColour(int row,int col,QString result,pair<QString,int> colColor){
    QString columnColor=colColor.first;
    ui->tableWidget_2->setItem(row,col,new QTableWidgetItem(result));
    if(columnColor.compare("RED")==0){
        ui->tableWidget_2->item(row, col)->setBackground(Qt::red);
    }
    else if(columnColor.compare("YELLOW")==0){
        ui->tableWidget_2->item(row, col)->setBackground(Qt::yellow);
    }
    else if(columnColor.compare("MAGENTA")==0){      //changed MAGENTA to magenta
        ui->tableWidget_2->item(row, col)->setBackground(Qt::magenta);
    }
    else{           //for white of no fill present
        //No colour
    }
}
void Beacon::updateColumnColor(int row,int col,QString result,pair<QString,int> colColor){
    QString columnColor=colColor.first;
    ui->tableWidget->setItem(row,col,new QTableWidgetItem(result));
    if(columnColor.compare("RED")==0){
        ui->tableWidget->item(row, col)->setBackground(Qt::red);
    }
    else if(columnColor.compare("YELLOW")==0){
        ui->tableWidget->item(row, col)->setBackground(Qt::yellow);
    }
    else if(columnColor.compare("MAGENTA")==0){      //changed MAGENTA to magenta
        ui->tableWidget->item(row, col)->setBackground(Qt::magenta);
    }
    else{           //for white of no fill present
        //No colour
    }
}
void Beacon::updateErrorTable(vector<pair<QString,ServerData>> &ServerErrorDataTable){    
    ui->tableWidget_2->clear();
    createTableFormat();
    int row=0;

    //First task is to generalize string that has to be further passed.
    vector<vector<pair<string,string>>> alaertFilteredData;

    vector<vector<string>> emailGenerator;         //this generates email to send
    for(auto it=ServerErrorDataTable.begin();it!=ServerErrorDataTable.end();++it){
        int col=0;
        vector<string> indvEmailData;

        ui->tableWidget_2->setItem(row,col,new QTableWidgetItem(it->second.ServerName));
        indvEmailData.push_back(it->second.ServerName.toStdString());
        col+=1;

        ui->tableWidget_2->setItem(row,col,new QTableWidgetItem(it->second.CityName));
        indvEmailData.push_back(it->second.CityName.toStdString());
        col+=1;

        ui->tableWidget_2->setItem(row,col,new QTableWidgetItem(it->second.IPaddress));
        indvEmailData.push_back(it->second.IPaddress.toStdString());
        col+=1;

        ui->tableWidget_2->setItem(row,col,new QTableWidgetItem(it->second.Version));
        indvEmailData.push_back(it->second.Version.toStdString());
        col+=1;

        //ui->tableWidget->setItem(row,col,new QTableWidgetItem(it->second.XMLversion));
        //col+=1;

        updateErrorColumnColour(row,col,it->second.StratusStatus,it->second.colColors["StratusColColor"]);
        indvEmailData.push_back(it->second.StratusStatus.toStdString());
        col+=1;

        updateErrorColumnColour(row,col,it->second.ALIStatus,it->second.colColors["ALIColColor"]);
        indvEmailData.push_back(it->second.ALIStatus.toStdString());
        col+=1;

        updateErrorColumnColour(row,col,it->second.TrunkStatus,it->second.colColors["TrunkColColor"]);
        indvEmailData.push_back(it->second.TrunkStatus.toStdString());
        col+=1;

        updateErrorColumnColour(row,col,it->second.PhoneStatus,it->second.colColors["PhoneColColor"]);
        indvEmailData.push_back(it->second.PhoneStatus.toStdString());
        col+=1;

        updateErrorColumnColour(row,col,it->second.WorkstationStatus,it->second.colColors["WorkStColColor"]);
        indvEmailData.push_back(it->second.WorkstationStatus.toStdString());
        row+=1;

        emailGenerator.push_back(indvEmailData);

        //Storing data for alerts

        vector<pair<string,string>> tmp;
        tmp.push_back({it->second.ServerName.toStdString(),"WHITE"});
        tmp.push_back({it->second.CityName.toStdString(),"WHITE"});
        tmp.push_back({it->second.IPaddress.toStdString(),"WHITE"});
        tmp.push_back({it->second.Version.toStdString(),"WHITE"});
        tmp.push_back({it->second.StratusStatus.toStdString(),it->second.colColors["StratusColColor"].first.toStdString()});
        tmp.push_back({it->second.ALIStatus.toStdString(),it->second.colColors["ALIColColor"].first.toStdString()});
        tmp.push_back({it->second.TrunkStatus.toStdString(),it->second.colColors["TrunkColColor"].first.toStdString()});
        tmp.push_back({it->second.PhoneStatus.toStdString(),it->second.colColors["PhoneColColor"].first.toStdString()});
        tmp.push_back({it->second.WorkstationStatus.toStdString(),it->second.colColors["WorkStColColor"].first.toStdString()});
        alaertFilteredData.push_back(tmp);
    }

    //This section is to send Alerts





    bool sendEmailFlag=false;

    if(fCheckAlarmTimeout()){
        if(emailCache!=emailGenerator)
            sendEmailFlag=true;
        emailCache  = emailGenerator;
        cSetTime();
    }

    //badically this will check if for 1 hours there is somrthing change that has occured then send email
     if(sendEmailFlag ) {       //sendEmailFlag
         //emailGenerator.push_back(vector<string> {to_string(ServerErrorDataTable.size())});
         //sendEmail(emailGenerator);
          sendAlertGeneralizeEmail(alaertFilteredData);
          wSetTime();
     }

    QString size = QString::number(ServerErrorDataTable.size());
    QString lable2Data = "Total Alarming Sites\n\n          "+size;
    ui->label_2->setText(lable2Data);
}

void Beacon:: readServerTable()
{
 //qDebug()<<"Reading file";
 QFile                      myFile("/home/mayank.srivastava/Beacon/ServerList.txt");
 QString                    line,name,city;
 int                        position;


if (!myFile.open(QIODevice::ReadOnly)) // Open the file
{
    qDebug()<<"Unable to open file";
    exit(1);
}

QTextStream stream( &myFile ); // Set the stream to read from myFile

 while ( !stream.atEnd() )
     {
      line = stream.readLine();
      position=line.indexOf("|");
      name = line.mid(0,position);
      city = line.mid(position+1,(line.length()-position));
      ServerDataTable[name].ServerName = name;
      ServerDataTable[name].CityName = city;
    }
    // Close the file
    myFile.close();
    //qDebug() << "finished reading file";
}

void Beacon:: printServerTable(){
    qDebug()<<"Total number of servers in file is "<<ServerDataTable.size();
    for(auto it=ServerDataTable.begin();it!=ServerDataTable.end();++it){
        qDebug()<<it->first<<" "<<it->second.ServerName<<" "<<it->second.CityName<<" "<<it->second.IPaddress<<" "<<it->second.Version;
    }
}

void Beacon::beginTimer()
{
 myTimerId = startTimer(DATA_UPDATE_THRESHOLD_MSEC);
}
int Beacon::getWeight(QString input)
{
    if(input.contains("RED"))
        return RED_COLOR_WEIGHT;
    if(input.contains("YELLOW"))
        return YELLOW_COLOR_WEIGHT;
    if(input.contains("MAGENTA"))
        return MAGENTA_COLOR_WEIGHT;

    return WHITE_COLOR_WEIGHT;

}
void Beacon::timerEvent(QTimerEvent *event)
{

  ServerDataMutex.lock();
  vector<pair<QString,ServerData>> ServerErrorDataTable;

  for(auto it=ServerDataTable.begin();it!=ServerDataTable.end();++it){

      //calculating total weight each time to sort the error table data and get based on the required priority
      int weight=0;
      weight+=getWeight(it->second.colColors["StratusColColor"].first);
      weight+=getWeight(it->second.colColors["ALIColColor"].first);
      weight+=getWeight(it->second.colColors["TrunkColColor"].first);
      weight+=getWeight(it->second.colColors["PhoneColColor"].first);
      weight+=getWeight(it->second.colColors["WorkStColColor"].first);
      it->second.TotalColWeight=weight;

      //Chaging NULl(empty) column to Not Applicable

      if(it->second.PhoneStatus=="")
          it->second.PhoneStatus="Not Applicable";
      if(it->second.TrunkStatus=="")
          it->second.TrunkStatus="Not Applicable";
      if(it->second.WorkstationStatus=="")
          it->second.WorkstationStatus="Not Applicable";
      if(it->second.ALIStatus=="")
          it->second.ALIStatus="Not Applicable";
      if(it->second.ServerStatus=="")
          it->second.ServerStatus="Not Applicable";

      if (it->second.fCheckforTimeout() || it->second.IPaddress.length()==0){           //it->second.IPaddress.length()==0   must be used if null is in the input changed to not applicable
          if(it->second.fCheckfor24hrsTimeout()){
              it->second.StratusStatus="24hrs No Connection";
              it->second.colColors["StratusColColor"]=ServerStatuslogicCheck("24HRS");
          }
          else{
          it->second.StratusStatus="No Connection";
          it->second.colColors["StratusColColor"]=ServerStatuslogicCheck("No Connection");
          }
          ServerErrorDataTable.push_back({it->first,it->second});
      }
      else if(it->second.TotalColWeight>0)
          ServerErrorDataTable.push_back({it->first,it->second});
  }


  //using lambda sorting to sort the ServerDataError table based on its wight
  sort(ServerErrorDataTable.begin(),ServerErrorDataTable.end(),[](pair<QString,ServerData> &one,pair<QString,ServerData> &two)
  {
     int weight1=one.second.TotalColWeight;
     int weight2=two.second.TotalColWeight;
     return weight1>weight2;
  });

  updateCompleteTable();            //This will update the bottom: all Sites table
  updateErrorTable(ServerErrorDataTable);   //This will update the upper: errors Sites table
  ServerDataMutex.unlock();

}

void Beacon::closeEvent(QCloseEvent *event)
{

    // cleanup here
    //boolSTOP_EXECUTION = true;
    sleep(1);
    event->accept();


}

