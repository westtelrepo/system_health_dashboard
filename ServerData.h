#ifndef SERVERDATA_H
#define SERVERDATA_H

#include<QString>
#include<QMutex>
#include<QElapsedTimer>
#include<iostream>
#include"linuxheader.h"
using namespace std;

class ServerData
{
 public:
 int        TableNumber;
 QString	ServerName;
 QString    CityName;
 QString	IPaddress;
 QString	StratusStatus;
 QString    PhoneStatus;
 QString    TrunkStatus;
 QString    ALIStatus;
 QString    WorkstationStatus;
 QString	XMLversion;
 QString	Version;
 QElapsedTimer      qTimeLastBeacon;
 bool		Expired;
 bool		HadBeenExpired;
 string     ServerStatus;
 map<QString,pair<QString,int>> colColors;                //here each col map to a vector --> <Color name, Color weight>
 int        TotalColWeight;

 ServerData()
 {
  TableNumber             = 0;
  ServerName              = "";
  CityName                = "";
  IPaddress               = "";
  StratusStatus           = "No Connection";
  PhoneStatus             = "";
  TrunkStatus             = "";
  ALIStatus               = "";
  XMLversion              = "";
  Version                 = "";
  WorkstationStatus       = "";
  qTimeLastBeacon.start();
  Expired                 = false;
  HadBeenExpired          = false;
  colColors               = {};
  TotalColWeight          = 0;
 }
 ~ServerData(){}

public:
     bool fCheckforTimeout();
     bool fCheckfor24hrsTimeout();
     void fSetTime();
     void fTimeoutAlarm();
     void fRestoredAlarm();
     bool InternetPingCheck();
     bool InternetPingCheckPre();
     QString seconds2time(int intArg);


};

//extern ServerData	ServerDataTable[MAX_ROWS];
//extern QMutex		ServerDataMutex;

#endif // SERVERDATA_H
