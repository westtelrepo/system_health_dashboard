QT       += core gui widgets xml network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    ServerData.cpp \
    alertgeneralizeemail.cpp \
    main.cpp \
    beacon.cpp \
    westtelemail.cpp

HEADERS += \
    ServerData.h \
    alertgeneralizeemail.h \
    beacon.h \
    linuxheader.h \
    westtelemail.h

FORMS += \
    beacon.ui

TRANSLATIONS += \
    Beacon_en_US.ts
CONFIG += lrelease
CONFIG += embed_translations
CONFIG += console

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    HeartBeatResource.qrc
