#ifndef LINUXHEADERS_H
#define LINUXHEADERS_H

#include<QApplication>
#include<QThread>
#include<QWidget>
#include<QMutex>
#include<QtGui>
#include<vector>
#include<map>
#include<unordered_map>
#include<algorithm>
#include<unistd.h>
#include<vector>
#include<iostream>
#include<sstream>
#include<fstream>
#include<string>

//#include "/home/westtel/ipworks_v9/include/ipworks.h"


#define EXPERIENT_HOME_BEACON_IP_ADDRESS   "64.207.47.218"
#define EXPERIENT_HOME_BEACON_PORT          59000
#define EXPERIENT_HOME_BEACON_INTERVAL_SEC  60
#define TIMEOUT_THRESHOLD_SEC               180
#define MILLISECONDS_PER_SECOND             1000
#define DATA_UPDATE_THRESHOLD_SEC            5
#define intEMAIL_TIMEOUT_SEC                 1
#define DATA_UPDATE_THRESHOLD_MSEC          900			    // milliseconds
#define MAX_ROWS                            5000			// number of rows in the table
#define NUM_COLUMNS                         5			    // number of Columns in the Table
#define MAX_SIZE_OF_HOSTNAME                20
#define PING_TIMEOUT                        1
#define PING_HOST_ONE                       "google.com"
#define PING_HOST_TWO                       "experient.com"
#define TIMEOUT_THRESHOLD_HRS               86400           //Theshhold to check for hours

//column colours weights
#define RED_COLOR_WEIGHT                    50000
#define MAGENTA_COLOR_WEIGHT                1000
#define YELLOW_COLOR_WEIGHT                 10
#define WHITE_COLOR_WEIGHT                  0



#endif // LINUXHEADERS_H

