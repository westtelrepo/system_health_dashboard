#include "westtelemail.h"



void WesttelEmail::wSetTime(){
   lastMailTime.restart();
}

void WesttelEmail::cSetTime(){
    lastChangeMailTime.restart();
}

bool WesttelEmail::fCheckforOneDayTimeout(){
    if (lastMailTime.elapsed() < (TIMEOUT_THRESHOLD_HRS*MILLISECONDS_PER_SECOND)){return false;}
    else                                                                            {return true;}
}

bool WesttelEmail::fCheckAlarmTimeout(){    //changed to an hour change
    if (lastChangeMailTime.elapsed() < (120000)){return false;}
    else                                                                            {return true;}
}
//7200*MILLISECONDS_PER_SECOND

/*
void WesttelEmail::sendEmail(vector<vector<string>> mailData){
    //Framing email format
    string generatedMailData;

    string header="PSAP                         PSAP Name                  IP Address               Controller Version       System Status            ALI Link Status         Trunk Status           Phone Status          Workstation Status";
    generatedMailData+=header+"\n\n\n";


    for(int i=0;i<mailData.size()-1;i++){
        for(int j=0;j<mailData[i].size();j++){
            if(mailData[i][j].length()<27){
                if(mailData[i][j].length()==0 || mailData[i][j].compare("Not Applicable")==0)
                    mailData[i][j]="NULL  ";
                int textlength=27-mailData[i][j].length();
                generatedMailData+=mailData[i][j];
                for(int k=0;k<textlength;k++)
                    generatedMailData+=" ";
            }
            else
                generatedMailData+=mailData[i][j];
        }
        generatedMailData+="\n\n";
    }
    //cout<<generatedMailData;
    generatedMailData+="\nTotal Alarming sites : "+mailData[mailData.size()-1][0];
    generatedMailData+="\n\n\nYours sincerely, \n WestTel System Health Monitor \n";
    generateFile(generatedMailData);

}
*/

string WesttelEmail::generateHTMLHeader(){
    string HTMLheader=" "
            "<!DOCTYPE html>"
            "<html>"
            "<head>"
            "<style>"
            "table, th, td {"
              "border: 1px solid black;"
              "border-collapse: collapse;"
              "padding: 15px;"
              "text-align: left"
            "}"
            "</style>"
            "</head>"
            "<body>"

            "<p><br/><br/>Hi, <br/>This email is getting generated from the Westtel Health Monitor system. Below is the list of PSAP having some issues <br/><br/><br/><br/></p>"


            "<table style='width:100%'>"
              "<colgroup>"
                    "<col span='1' style='background-color:lightgray'>"
                    "<col style='background-color:white'>"
              "</colgroup>"
              "<tr style='background-color: lightblue'>"
                "<th>PSAP</th>"
                "<th>PSAP NAME</th> "
                "<th>IP Address</th>"
                "<th>Controller Version</th>"
                "<th>System Status</th>"
                "<th>ALI Status</th>"
                "<th>Trunk Status</th>"
                "<th>Phone Status</th>"
                "<th>Workstation Status</th>"
             "</tr>";
    return HTMLheader;
}
string WesttelEmail::generateValueInHTML(vector<vector<string>> mailData){
    vector<vector<string>> individualTableTagsPerSites(mailData.size()-1);
    for(int i=0;i<mailData.size()-1;i++){
        individualTableTagsPerSites[i].push_back("<tr>");
        for(int j=0;j<mailData[i].size();j++){
            individualTableTagsPerSites[i].push_back("<td>");
            individualTableTagsPerSites[i].push_back(mailData[i][j]);
            individualTableTagsPerSites[i].push_back("</td>");
        }
        individualTableTagsPerSites[i].push_back("</tr>");
    }


    //Converting all individual data to one string
    string result="";
    for(int i=0;i<individualTableTagsPerSites.size();i++){
       for(int j=0;j<individualTableTagsPerSites[i].size();j++){
           result = result + individualTableTagsPerSites[i][j];
       }
    }
    //Adding the total alarming HTML code
    result = result + "</table><br/><br/><h4>Total Alarming Sites Reported = "+to_string(mailData.size()-1)+"</h4><br/>";

    //Adding Thank you message
    result = result + "<p>Yours sincerely, <br/> WestTel System Health Monitor</p>";

    //Closuere of the HTMl
    result = result + "</body></html>";

    return result;
}
void WesttelEmail::sendEmail(vector<vector<string>> mailData){
    string headerHTML=generateHTMLHeader();
    string valuesINHTML=generateValueInHTML(mailData);
    string generatedMailData=headerHTML+valuesINHTML;
    generateFile(generatedMailData);

}
void WesttelEmail::generateFile(string data){
    string senderEmail="heartbeat.monitor@westtel.com";
    //string senderEmail="mayank.srivastava@westtel.com";
    string from="mayank.srivastava@westtel.com";
    string subject="[ALERT]: HeartBeat Monitor";
    string format="Content-Type: text/html; charset='utf8'\n\n";
    string fileName="mail.txt";

    string fsender="To:"+senderEmail+"\n";
    string freceiver="From:"+from+"\n";
    string fsubject="subject:"+subject+"\n";
    data = fsender + freceiver + fsubject + format + data;
    ofstream file;
    file.open(fileName);
    file << data;
    file.close();
    generateSMTPcommand(senderEmail,fileName);
}
void WesttelEmail::generateSMTPcommand(string senderEmail,string filename){
    string command = "ssmtp "+senderEmail +"< "+filename;
    const char *finalCommand = command.c_str();
    system(finalCommand);
    //exit(0);
}

QString WesttelEmail::seconds2time(int intArg)
{
 int                    intHours,intMin,intSec;
 QString                strOutput, strNumber;


 intHours = (int(intArg)/3600);
 intMin   = (int(int(intArg)%3600)/60);
 intSec   = (int(int(intArg)%3600)%60);
 if (intHours)                                  {strOutput = strNumber.setNum(intHours);}
 if (intHours > 1)                              {strOutput += " Hours";}
 else if (intHours == 1)                        {strOutput += " Hour";}
 if (((intMin>0)||(intSec>0))&& (intHours > 0)) {strOutput += ", ";}

 if ((((intMin>0)||(intSec>0))&&(intHours>0))||(intMin>0))
  {
   strOutput += strNumber.setNum(intMin);
   if(intMin == 1)                              {strOutput += " Minute";}
   else                                         {strOutput += " Minutes";}
   if (intSec)                                  {strOutput += " and ";}
  }

 if (intSec)
  {
   strOutput += strNumber.setNum(intSec);;
   if (intSec == 1)                             {strOutput += " Second";}
   else                                         {strOutput += " Seconds";}
  }

 return strOutput;

}// end seconds2time(int intArg)
