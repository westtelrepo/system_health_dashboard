﻿#include "ServerData.h"

bool ServerData::InternetPingCheckPre(){
    QProcess pingProcess;
    QString exec = "ping";
    QStringList params;
    params << "-c" << "1" << PING_HOST_ONE;
    pingProcess.start(exec,params,QIODevice::ReadOnly);
    pingProcess.waitForFinished(-1);

    QString p_stdout = pingProcess.readAllStandardOutput();
    QString p_stderr = pingProcess.readAllStandardError();

    if(p_stderr.capacity()==0)
        return true;
    return false;
}
void ServerData::fSetTime(){
   qTimeLastBeacon.restart();
}
bool ServerData::fCheckforTimeout( ){
   if (qTimeLastBeacon.elapsed() < (TIMEOUT_THRESHOLD_SEC*MILLISECONDS_PER_SECOND)){return false;}
   else                                                                            {return true;}
}
bool ServerData::fCheckfor24hrsTimeout(){
    if (qTimeLastBeacon.elapsed() < (TIMEOUT_THRESHOLD_HRS*MILLISECONDS_PER_SECOND)){return false;}
    else                                                                            {return true;}
}

QString seconds2time(int intArg)
{
 int                    intHours,intMin,intSec;
 QString                strOutput, strNumber;


 intHours = (int(intArg)/3600);
 intMin   = (int(int(intArg)%3600)/60);
 intSec   = (int(int(intArg)%3600)%60);
 if (intHours)                                  {strOutput = strNumber.setNum(intHours);}
 if (intHours > 1)                              {strOutput += " Hours";}
 else if (intHours == 1)                        {strOutput += " Hour";}
 if (((intMin>0)||(intSec>0))&& (intHours > 0)) {strOutput += ", ";}

 if ((((intMin>0)||(intSec>0))&&(intHours>0))||(intMin>0))
  {
   strOutput += strNumber.setNum(intMin);
   if(intMin == 1)                              {strOutput += " Minute";}
   else                                         {strOutput += " Minutes";}
   if (intSec)                                  {strOutput += " and ";}
  }

 if (intSec)
  {
   strOutput += strNumber.setNum(intSec);;
   if (intSec == 1)                             {strOutput += " Second";}
   else                                         {strOutput += " Seconds";}
  }

 return strOutput;

}// end seconds2time(int intArg)
