#ifndef ALERTGENERALIZEEMAIL_H
#define ALERTGENERALIZEEMAIL_H
#include "linuxheader.h"
using namespace std;
class AlertGeneralizeEmail
{
public:
    void sendAlertGeneralizeEmail(vector<vector<pair<string,string>>>&);
    string generateValueInHTML(vector<vector<pair<string,string>>>&);
    string generateHTMLHeader();
    void generateFile(string);
    void generateSMTPcommand(string,string);
    AlertGeneralizeEmail();
};

#endif // ALERTGENERALIZEEMAIL_H
