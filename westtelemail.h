#ifndef WESTTELEMAIL_H
#define WESTTELEMAIL_H

#include<QElapsedTimer>
#include<QString>
#include "linuxheader.h"

using namespace std;
class WesttelEmail
{
public:
    bool enableMail;
    QElapsedTimer lastMailTime;
    QElapsedTimer lastChangeMailTime;
    vector<vector<string>> emailCache;

    WesttelEmail(){
        enableMail = true;
        lastMailTime.start();
        emailCache = {};
        lastChangeMailTime.start();
    }
    ~WesttelEmail(){};
public:
    void sendEmail(vector<vector<string>>);
    void generateFile(string);
    void generateSMTPcommand(string,string);
    bool fCheckforOneDayTimeout();
    bool fCheckAlarmTimeout();
    void wSetTime();
    void cSetTime();
    void fTimeoutAlarm();
    void fRestoredAlarm();
    string generateHTMLHeader();
    string generateValueInHTML(vector<vector<string>>);
    QString seconds2time(int intArg);
};

#endif // WESTTELEMAIL_H
