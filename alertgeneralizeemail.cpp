#include "alertgeneralizeemail.h"

AlertGeneralizeEmail::AlertGeneralizeEmail()
{

}
void AlertGeneralizeEmail::sendAlertGeneralizeEmail(vector<vector<pair<string,string>>>& filteredErrorData){
    //Start of email for alerts
    string headerHTML=generateHTMLHeader();
    string valuesINHTML=generateValueInHTML(filteredErrorData);
    string generatedMailData=headerHTML+valuesINHTML;
    generateFile(generatedMailData);
}
void AlertGeneralizeEmail::generateFile(string data){
    //string senderEmail="heartbeat.monitor@westtel.com";
    string senderEmail="mayank.srivastava@westtel.com";
    string from="mayank.srivastava@westtel.com";
    string subject="[ALERT]: WestTel System Health Monitor";
    string format="Content-Type: text/html; charset='utf8'\n\n";
    string fileName="mail.txt";

    string fsender="To:"+senderEmail+"\n";
    string freceiver="From:"+from+"\n";
    string fsubject="subject:"+subject+"\n";
    data = fsender + freceiver + fsubject + format + data;
    ofstream file;
    file.open(fileName);
    file << data;
    file.close();
    generateSMTPcommand(senderEmail,fileName);
}
void AlertGeneralizeEmail::generateSMTPcommand(string senderEmail,string filename){
    string command = "ssmtp "+senderEmail +"< "+filename;
    const char *finalCommand = command.c_str();
    system(finalCommand);
    exit(0);
}
string AlertGeneralizeEmail::generateValueInHTML(vector<vector<pair<string,string>>>& filteredErrorData){

    int x=0;
    vector<vector<string>> individualTableTagsPerSites(filteredErrorData.size());
    for(int i=0;i<filteredErrorData.size();i++){
        individualTableTagsPerSites[x].push_back("<tr>");
        for(int j=0;j<filteredErrorData[i].size();j++){
            string col="'"+filteredErrorData[i][j].second+"'";
            individualTableTagsPerSites[x].push_back("<td bgcolor=" + col + ">");
            individualTableTagsPerSites[x].push_back(filteredErrorData[i][j].first);
            individualTableTagsPerSites[x].push_back("</td>");
        }
        individualTableTagsPerSites[x].push_back("</tr>");
        x++;
    }


    //Converting all individual data to one string
    string result="";
    for(int i=0;i<individualTableTagsPerSites.size();i++){
       for(int j=0;j<individualTableTagsPerSites[i].size();j++){
           result = result + individualTableTagsPerSites[i][j];
       }
    }
    //Adding the total alarming HTML code
    result = result + "</table><br/><br/><h4>Total Alarming Sites Reported = "+to_string(filteredErrorData.size())+"</h4><br/>";

    //Adding colour weight
    result =result +  "<p>"
            "<h4>Color Priority</h4>"
            "<ul>"
                "<li>Red: HIGH</li>"
                "<li>Yellow: MEDIUM</li>"
                "<li>White: No Alert </li>"
            "</ul>"
            "</p><br />";

    //Adding Thank you message
    result = result + "<p style='font-size:15px'><b>Yours sincerely,</b> <br/> WestTel System Health Monitor</p>";

    //Closuere of the HTMl
    result = result + "</body></html>";

    return result;
}
string AlertGeneralizeEmail::generateHTMLHeader(){
    string HTMLheader=" "
            "<!DOCTYPE html>"
            "<html>"
            "<head>"
            "<style>"
            "table, th, td {"
              "border: 1px solid black;"
              "border-collapse: collapse;"
              "padding: 15px;"
              "text-align: left"
            "}"
            "</style>"
            "</head>"
            "<body>"

            "<p style='font-size:15px'><br/><br/>Hi, <br/>This email is getting generated from the <b>Westtel System Health Monitor</b>. Below is the list of PSAP having some issues <br/><br/><br/><br/></p>"


            "<table style='width:100%'>"
              "<colgroup>"
                    "<col span='1' style='background-color:lightgray'>"
                    "<col style='background-color:white'>"
              "</colgroup>"
              "<tr style='background-color: lightblue'>"
                "<th>PSAP</th>"
                "<th>PSAP NAME</th> "
                "<th>IP Address</th>"
                "<th>Controller Version</th>"
                "<th>System Status</th>"
                "<th>ALI Status</th>"
                "<th>Trunk Status</th>"
                "<th>Phone Status</th>"
                "<th>Workstation Status</th>"
             "</tr>";
    return HTMLheader;
}
